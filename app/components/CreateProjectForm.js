import React from "react";
import {MDBInput} from "mdbreact";
import { apiService} from "../utils/ApiHandler";
import routes from "../constants/routes";

export default class CreateProjectForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          loading: false, projectName: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleChange(e){
        this.setState({[e.target.name]: e.target.value})
    }


    onSubmit(e){
        e.preventDefault();
        this.setState({loading: true});
        if ( this.state.projectName === ''){
            return;
        }
        apiService.createProject(this.state.projectName).then(resp => {
            console.log(resp.data);
            this.setState({loading: false});
            this.props.history.push(routes.PROJECTS_LIST);
        });
    }



    render(){
        if (this.state.loading) return <div className="loader loader-sm" />;
        return (
            <form onSubmit={ this.onSubmit }>
                <MDBInput
                    label="Project Name" name="projectName"
                    group
                    type="text"
                    validate
                    error="wrong"
                    success="right" onChange={this.handleChange}
                    />
                <button className="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0"
                        type="submit">
                    Create
                </button>
            </form>
        );
    }
}