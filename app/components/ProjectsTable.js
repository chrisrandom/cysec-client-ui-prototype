import React from "react";
import routes from '../constants/routes';
import {withRouter} from "react-router-dom";

class ProjectsTable extends React.Component<Props> {
    props: Props;

    setActiveProject(project){
        this.props.setActiveProject(project);
    }

    render(){
        console.log(this.props);
        return(
            <table className="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Project</th>
                        <th>Project ID</th>
                        <th>Start Date</th>
                        <th>Due Date</th>
                        <th>Hosts</th>
                        <th>Services</th>
                        <th>Vulnerabilities</th>
                        <th>Owners</th>
                    </tr>
                </thead>
                <tbody>
                 {this.props.projects.map((project, i) =>
                      <tr key={project.id}>
                          <td onClick={() => {this.setActiveProject(project)}}>{project.name}</td>
                          <td>{project.slug}</td>
                          <td>{project.start_date}</td>
                          <td>{project.due_date}</td>
                          <td>{project.hosts}</td>
                          <td>{project.services}</td>
                          <td>{project.vulnerabilities}</td>
                          <td>{project.owners}</td>
                      </tr>
                    )}
                </tbody>
            </table>
        )
    }
}

export default withRouter(ProjectsTable);