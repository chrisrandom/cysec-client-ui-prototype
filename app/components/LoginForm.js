import React from "react";
import { Redirect } from 'react-router-dom';
import { MDBInput } from "mdbreact";
import { apiService } from '../utils/ApiHandler';

export default class LoginForm extends React.Component {


    constructor(props){
        super(props);
        this.state = {
            redirectToReferrer: false, username: '', password: '',
            api_host: 'http://localhost:8000/api', loading: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }



    handleSubmit(e){
        e.preventDefault();
        this.setState({ loading: true});
        apiService.login(this.state.username, this.state.password, this.state.api_host).then(user => {
            this.setState({redirectToReferrer: true, loading: false});
        }).catch(error => {
            if(error && error.response && error.response.status === 400){
                this.props.errorOccured(error.response.data.non_field_errors[0]);
            }else{
                this.props.errorOccured("unknown error occured");
            }
            this.setState({loading: false});
        });
    }

    handleChange(e){
        this.props.errorOccured('');
        this.setState({[e.target.name]: e.target.value})
    }


    render(){
        const { from } = { from: { pathname: "/" } };
        const { redirectToReferrer } = this.state;

        if (redirectToReferrer) return <Redirect to={from} />;
        if (this.state.loading) return <div className="loader loader-small mx-auto mt-3" />;
        return(
            <form className="text-center" style={{color : "#757575"}} onSubmit={this.handleSubmit}>
                <MDBInput
                    label="Username" name="username"
                    group
                    type="text"
                    validate
                    error="wrong"
                    success="right" onChange={this.handleChange}
                  />
                <MDBInput
                    label="Password" name="password"
                    group
                    type="password"
                    validate onChange={this.handleChange}
                />

                <MDBInput
                    label="API-Host" group type="url" name="api_host" validate value="http://localhost:8000/api" onChange={this.handleChange}
                />

                <div className="d-flex justify-content-around">
                    <div>
                        <a href="#">Forgot password?</a>
                    </div>
                </div>

                <button className="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0"
                        type="submit" disabled={this.state.loading}>
                    Sign in
                </button>

                <p>Not a member?
                    <a href="#">Register</a>
                </p>
            </form>
        )
    }
}