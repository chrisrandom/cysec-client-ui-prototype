import React from "react";
import { Redirect } from 'react-router';
import ProjectsTable from "./ProjectsTable";
import {apiService} from "../utils/ApiHandler";
import routes from '../constants/routes';

export default class Projects extends React.Component<Props> {

    props: Props;

    constructor(props){
        super(props);
        this.state = {
            projects: [],
        };

    }

    componentDidMount(): void {
        //this.props.setActiveProject(null, null);
        apiService.getProjects().then(resp => {
           const projects = resp.data.results;
           this.setState({ projects });
        });
    }


    render(){
        if(this.props.activeProject){
            return(<Redirect to={routes.PROJECT_DASHBOARD} />);
        }
        return(
            <div className="container">
                <div className="col-sm-12">
                    <h3 className="text-center">Projects</h3>
                    <hr/>
                    <ProjectsTable projects={ this.state.projects } setActiveProject={this.props.setActiveProject} />
                </div>
            </div>
        )
    }
}