// @flow
import { Menu, shell, ipcMain } from 'electron';
import { apiService } from './utils/ApiHandler';

export default class MenuBuilder {

    constructor(mainWindow) {
        this.mainWindow = mainWindow;
    }

    buildMenu() {
        if (
            process.env.NODE_ENV === 'development' ||
            process.env.DEBUG_PROD === 'true'
        ) {
            this.setupDevelopmentEnvironment();
        }

        const template = this.buildDefaultTemplate();

        const menu = Menu.buildFromTemplate(template);
        Menu.setApplicationMenu(menu);

        return menu;
    }

    setupDevelopmentEnvironment() {
        this.mainWindow.openDevTools();
        this.mainWindow.webContents.on('context-menu', (e, props) => {
            const { x, y } = props;

            Menu.buildFromTemplate([
                {
                    label: 'Inspect element',
                    click: () => {
                        this.mainWindow.inspectElement(x, y);
                    }
                }
            ]).popup(this.mainWindow);
        });
    }

    buildDefaultTemplate() {
        const templateDefault = [
            {
                label: '&File',
                submenu: [
                    {
                        label: '&New Project',
                        accelerator: 'Ctrl+O',
                        click: () => {
                            this.mainWindow.webContents.send("open-create-project-page");
                        }
                    },
                    {
                        label: '&Switch Project',
                        click: () => {
                            this.mainWindow.webContents.send("open-projects-list-page");
                        }
                    },
                    {
                        label: '&Quit',
                        accelerator: 'Ctrl+W',
                        click: () => {
                            this.mainWindow.close();
                        }
                    }
                ]
            },
            {
                label: 'Bug-Hunting',
                submenu: [
                    { label: 'Publish' }
                ]
            },
            {
                label: 'Accounts',
                submenu: [
                    {
                        label: 'Logout',
                        click: () => {
                            this.mainWindow.webContents.send("logout-user");
                        }
                    }
                ]
            },
            {
                label: 'Help',
                submenu: [
                    {
                        label: 'Documentation',
                        click() {
                            shell.openExternal(
                                'https://teahub.io/cysec/CySec-Server/wiki'
                            );
                        }
                    },
                    {
                        label: 'Search Issues',
                        click() {
                            shell.openExternal('https://teahub.io/cysec/cysec-server/issues');
                        }
                    }
                ]
            }
        ];
        return templateDefault;
    }
}
