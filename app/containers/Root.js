// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import type { Store } from '../reducers/types';
import Routes from '../Routes';
import routes from '../constants/routes';

import { ipcRenderer } from "electron"

type Props = {
  store: Store,
  history: {}
};

export default class Root extends Component<Props> {

  componentDidMount(): void {

      ipcRenderer.on("logout-user", () =>{
          localStorage.removeItem("user");
          this.props.history.push(routes.LOGIN);
      });

      ipcRenderer.on("open-projects-list-page", () => {
            this.props.history.push(routes.PROJECTS_LIST);
      });

      ipcRenderer.on("open-create-project-page", () => {
         this.props.history.push(routes.CREATE_PROJECT);
      });
  }

    render() {
    const { store, history } = this.props;
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Routes />
        </ConnectedRouter>
      </Provider>
    );
  }
}
