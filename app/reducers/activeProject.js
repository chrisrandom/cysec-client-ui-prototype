// @flow

import { SET_ACTIVE_PROJECT } from '../actions/projects';
import type { Action } from './types';

export default function activeProject(state: object = null, action: Action) {
  switch (action.type) {
    case SET_ACTIVE_PROJECT:
      return action.project;
    default:
      return state;
  }
}
