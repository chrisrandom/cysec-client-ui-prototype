// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import activeProject from './activeProject';

export default function createRootReducer(history: History) {
    return combineReducers({
        router: connectRouter(history),
        activeProject,
    });
}
