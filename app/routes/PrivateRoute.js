import React from "react";
import { Route, Redirect } from "react-router-dom";
import routes from '../constants/routes';

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render = {props => (
      localStorage.getItem('user')
      ? <Component {...props} />
      : <Redirect to={{ pathname: routes.LOGIN, state: {from: props.location}}}/>
    )} />
);
