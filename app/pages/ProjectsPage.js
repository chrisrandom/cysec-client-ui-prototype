import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Projects from '../components/Projects';
import * as ProjectsActions from '../actions/projects';

function mapStateToProps(state) {
  return {
    activeProject: state.activeProject
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ProjectsActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Projects);
