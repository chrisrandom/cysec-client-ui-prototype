import React from "react";
import CreateProjectForm from "../components/CreateProjectForm";


export default class CreateProjectPage extends React.Component<Props> {

    render(){
        return(
            <CreateProjectForm history={ this.props.history }/>
        );
    }
}