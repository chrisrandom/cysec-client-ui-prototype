import React from "react";
import LoginForm from "../components/LoginForm";

export default class LoginPage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            error: ''
        }
    }

    errorOccured(error){
        this.setState({
           error
        });
    }

    display(){
        if(this.state.error === ''){
            return <LoginForm errorOccured={this.errorOccured.bind(this)}/>
        }
        return <div><div className="alert alert-danger">{this.state.error}</div><LoginForm errorOccured={this.errorOccured.bind(this)}/></div>
    }

    render(){
        return(
            <header>
                <div className="view">
                    <div className="mask rgba-gradient d-flex justify-content-center align-items-center">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8 mx-auto mt-xl-5 wow fadeInRight" data-wow-delay="0.3s">
                                    <div className="card">
                                        <h5 className="card-header info-color white-text text-center py-4">
                                            <strong>Sign in</strong>
                                        </h5>

                                        <div className="card-body px-lg-5 pt-0">
                                            { this.display() }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
