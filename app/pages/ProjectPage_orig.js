import { connect } from 'react-redux';
import Project from '../components/Project';

function mapStateToProps(state) {
  return {
    activeProject: state.activeProject
  };
}

export default connect(
  mapStateToProps,
)(Project);
