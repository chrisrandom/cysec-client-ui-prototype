import React from "react";
import { Terminal } from 'xterm';
import os from 'os';
var pty = require('node-pty');
import TerminalEmulator from '../components/TerminalEmulator';

export default class ProjectPage extends React.Component<Props>{

    constructor(props){
        super(props);

        this.shell = process.env[os.platform() === 'win32' ? 'COMSPEC': 'SHELL'];
        this.ptyProcess = pty.spawn(this.shell, [], {
            name: 'xterm-color',
            cols: 80,
            rows: 30,
            cwd: process.cwd(),
            env: process.env
        });
        this.xterm = new Terminal();
    }

    componentDidMount(): void {
        this.xterm.open(document.getElementById('terminal'));

        this.xterm.on('data', (data) => {
           this.ptyProcess.write(data);
        });

        this.ptyProcess.on('data', (data) => {
            this.xterm.write(data);
        });
    }


    render(){
        return (
            <TerminalEmulator />
        )
    }

}
