import React from 'react';
import { Switch, Route } from 'react-router';
import { PrivateRoute } from './routes/PrivateRoute';
import routes from './constants/routes';
import App from './containers/App';
import LoginPage from "./pages/LoginPage";
import ProjectsPage from "./pages/ProjectsPage";
import ProjectPage from './pages/ProjectPage';
import CreateProjectPage from "./pages/CreateProjectPage";

export default () => (
    <App>
        <Switch>
            <Route exact path={routes.LOGIN} component={LoginPage} />
            <PrivateRoute exact path={routes.PROJECT_DASHBOARD} component={ProjectPage} />
            <PrivateRoute exact path={routes.HOME} component={ProjectsPage} />
            <PrivateRoute exact path={routes.PROJECTS_LIST} component={ProjectsPage} />
            <PrivateRoute exact path={routes.CREATE_PROJECT} component={CreateProjectPage} />
        </Switch>
    </App>
);
