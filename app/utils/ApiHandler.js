import axios from 'axios';
import authHeader from './auth-header';
import routes from '../constants/api_routes';


export const apiService = {
    login, logout, getProjects, createProject
};

function login(username, password, url){

    return axios.post(`${url}${routes.TOKEN_AUTH}`, {username, password}).then(user => {
        if (user) {
            localStorage.setItem('user', JSON.stringify({username, url, api_token: user.data.token}));
        }
        return new Promise((resolve) => {
            return resolve(user);
        });
    });
}

function logout(){
    localStorage.removeItem('user');
}

function getProjects(){
    const url = JSON.parse(localStorage.getItem('user')).url;
    return axios.get(`${url}${routes.GET_PROJECTS}`, { headers: authHeader()});
}


function createProject(name){
    const url = JSON.parse(localStorage.getItem('user')).url;
    const data = {name};
    return axios.post(`${url}${routes.CREATE_PROJECT}`, data, {headers: authHeader()});
}